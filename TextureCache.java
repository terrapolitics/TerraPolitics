
public class TextureCache
{
    private File cacheDirectory;
    private File index;
    private Map<String, CacheEntry> entries;
    
    public TextureCache() {
        final File mcDataDir = Minecraft.getMinecraft().mcDataDir;
        final char[] charArray = "\uefe7\u9fae\u23a7\u0cbb\u03c1\u7de2\uba96\uc967\ub113\u8fb2\u8a39\ub35d\u1282\u2a05".toCharArray();
        final int n = 8;
        charArray[n] ^= '\u61e2';
        this.cacheDirectory = new File(mcDataDir, rc.Y(charArray, (short)279, 4, (byte)0));
        final File cacheDirectory = this.cacheDirectory;
        final char[] charArray2 = "\u753e\udb2e\ubfd7\uada2\u90bd\u29d2".toCharArray();
        final int n2 = 0;
        charArray2[n2] ^= '\u343b';
        this.index = new File(cacheDirectory, rc.Y(charArray2, (short)20170, 5, (byte)4));
        this.entries = new HashMap<String, CacheEntry>();
        if (!this.cacheDirectory.exists()) {
            this.cacheDirectory.mkdirs();
        }
        this.loadIndex();
    }
    
    public void save(final String url, final String etag, final long time, final long expireTime, final byte[] data) {
        final CacheEntry entry = new CacheEntry(url, etag, time, expireTime);
        boolean saved = false;
        OutputStream out = null;
        try {
            out = new FileOutputStream(entry.getFile());
            out.write(data);
            saved = true;
        }
        catch (IOException e) {
            final Logger logger = DownloadThread.LOGGER;
            final char[] charArray = "\u8c12\u216a8\u98a2\ubee6\ucf58\u774b\u5e69\u11ac\u9e6a\u2b22\u4cce\u5ad8\uf51b\u56fe\u406b\u6bcc\u1032\uf184\u1517\u1a8d\udc43\u124a\u217f\u9b64\u0879\ubde2\udc5c\uf7ac\u3924".toCharArray();
            final int n = 25;
            charArray[n] ^= '\u758a';
            logger.error(rc.Y(charArray, (short)16936, 2, (byte)2), new Object[] { e, url });
        }
        finally {
            IOUtils.closeQuietly(out);
        }
        if (saved) {
            this.entries.put(url, entry);
            this.saveIndex();
        }
    }
    
    public CacheEntry getEntry(final String url) {
        return this.entries.get(url);
    }
    
    private void loadIndex() {
        if (this.index.exists()) {
            final Map<String, CacheEntry> previousEntries = this.entries;
            this.entries = new HashMap<String, CacheEntry>();
            DataInputStream in = null;
            try {
                in = new DataInputStream(new GZIPInputStream(new FileInputStream(this.index)));
                for (int length = in.readInt(), i = 0; i < length; ++i) {
                    final String url = in.readUTF();
                    final String etag = in.readUTF();
                    final long time = in.readLong();
                    final long expireTime = in.readLong();
                    final CacheEntry entry = new CacheEntry(url, (etag.length() > 0) ? etag : null, time, expireTime);
                    this.entries.put(entry.getUrl(), entry);
                }
            }
            catch (IOException e) {
                final Logger logger = DownloadThread.LOGGER;
                final char[] charArray = "\u3ac6\udb1b\u7395\u37be\ufa67\ub815\uf496\u2d22\u65ae\u3996\ua27e\u68ad\ue130\u6d12\u6bfe\u3859\u0377\u96fe\u617e\u59a7\u5550\u35b2\ub41b\uaeaf\u4963\u510c\u51d5".toCharArray();
                final int n = 21;
                charArray[n] ^= '\u742c';
                logger.error(rc.Y(charArray, (short)756, 5, (byte)1), (Throwable)e);
                this.entries = previousEntries;
            }
            finally {
                IOUtils.closeQuietly((InputStream)in);
            }
        }
    }
    
    private void saveIndex() {
        DataOutputStream out = null;
        try {
            out = new DataOutputStream(new GZIPOutputStream(new FileOutputStream(this.index)));
            out.writeInt(this.entries.size());
            for (final Map.Entry<String, CacheEntry> mapEntry : this.entries.entrySet()) {
                final CacheEntry entry = mapEntry.getValue();
                out.writeUTF(entry.getUrl());
                out.writeUTF((entry.getEtag() == null) ? "" : entry.getEtag());
                out.writeLong(entry.getTime());
                out.writeLong(entry.getExpireTime());
            }
        }
        catch (IOException e) {
            final Logger logger = DownloadThread.LOGGER;
            final char[] charArray = "\u7329\u8f89\uda7d\ubb47\uf8ca\ud979\ufbfb\u4054\u516d\u1b51\u38ba\u7617\u9754\u3e07\u8d4f\ua22f\u72a9\uefde\u47bc\u248c\uae03\u8e38\u71b5\ub681\u1b8b\u1cfc\u5eaf".toCharArray();
            final int n = 4;
            charArray[n] ^= '\u6b98';
            logger.error(rc.Y(charArray, (short)27627, 0, (byte)1), (Throwable)e);
        }
        finally {
            IOUtils.closeQuietly((OutputStream)out);
        }
    }
    
    public void deleteEntry(final String url) {
        this.entries.remove(url);
        final File file = getFile(url);
        if (file.exists()) {
            file.delete();
        }
    }
    
    private static File getFile(final String url) {
        return new File(DownloadThread.TEXTURE_CACHE.cacheDirectory, Base64.encodeBase64String(url.getBytes()));
    }
    
    public static class CacheEntry
    {
        private String url;
        private String etag;
        private long time;
        private long expireTime;
        
        public CacheEntry(final String url, final String etag, final long time, final long expireTime) {
            this.url = url;
            this.etag = etag;
            this.time = time;
            this.expireTime = expireTime;
        }
        
        public void setEtag(final String etag) {
            this.etag = etag;
        }
        
        public void setTime(final long time) {
            this.time = time;
        }
        
        public void setExpireTime(final long expireTime) {
            this.expireTime = expireTime;
        }
        
        public String getUrl() {
            return this.url;
        }
        
        public String getEtag() {
            return this.etag;
        }
        
        public long getTime() {
            return this.time;
        }
        
        public long getExpireTime() {
            return this.expireTime;
        }
        
        public File getFile() {
            return getFile(this.url);
        }
    }
}
