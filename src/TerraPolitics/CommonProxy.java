
public class CommonProxy
{
    public static final InventorySaveHandler antidropHandler;
    
    public void preInit(final FMLPreInitializationEvent event) {
    }
    
    public void Init(final FMLInitializationEvent event) {
        final Side var10000 = event.getSide();
        FMLCommonHandler.instance().getSide();
        if (var10000 == Side.SERVER) {
            MinecraftForge.EVENT_BUS.register((Object)CommonProxy.antidropHandler);
            FMLCommonHandler.instance().bus().register((Object)CommonProxy.antidropHandler);
            MinecraftForge.EVENT_BUS.register((Object)new Xenobyte());
        }
    }
    
    public void postInit(final FMLPostInitializationEvent event) {
    }
    
    public void registerRenderers() {
    }
    
    public void registerNEIStuff() {
    }
    
    public void serverStarting(final FMLServerStartingEvent event) {
        event.registerServerCommand((ICommand)new CommandGetSerial());
        event.registerServerCommand((ICommand)new CommandHandler());
    }
    
    static {
        antidropHandler = new InventorySaveHandler();
    }
}
