public class ei<K, V>
{
    private final LinkedHashMap<K, V> map;
    private int size;
    private int maxSize;
    private int putCount;
    private int createCount;
    private int evictionCount;
    private int hitCount;
    private int missCount;
    
    public ei(final int maxSize) {
        if (maxSize <= 0) {
            throw new IllegalArgumentException("maxSize <= 0");
        }
        this.maxSize = maxSize;
        this.map = new LinkedHashMap<K, V>(0, 0.75f, true);
    }
    
    public final synchronized V get(final K k) {
        if (k == null) {
            throw new NullPointerException("key == null");
        }
        final V value = this.map.get(k);
        if (value != null) {
            ++this.hitCount;
            return value;
        }
        ++this.missCount;
        final V create = this.create(k);
        if (create != null) {
            ++this.createCount;
            this.size += this.safeSizeOf(k, create);
            this.map.put(k, create);
            this.trimToSize(this.maxSize);
        }
        return create;
    }
    
    public final synchronized void put(final K key, final V value) {
        if (key == null || value == null) {
            throw new NullPointerException("km");
        }
        ++this.putCount;
        this.size += this.safeSizeOf(key, value);
        final V put = this.map.put(key, value);
        if (put != null) {
            this.size -= this.safeSizeOf(key, put);
        }
        this.trimToSize(this.maxSize);
    }
    
    private void trimToSize(final int n) {
        while (this.size > n && !this.map.isEmpty()) {
            final Map.Entry<K, V> entry = this.map.entrySet().iterator().next();
            if (entry == null) {
                break;
            }
            final K key = entry.getKey();
            final V value = entry.getValue();
            this.map.remove(key);
            this.size -= this.safeSizeOf(key, value);
            ++this.evictionCount;
            this.entryEvicted(key, value);
        }
        if (this.size < 0 || (this.map.isEmpty() && this.size != 0)) {
            throw new IllegalStateException(this.getClass().getName() + ".s!");
        }
    }
    
    public final synchronized V remove(final K key) {
        if (key == null) {
            throw new NullPointerException("");
        }
        final V remove = this.map.remove(key);
        if (remove != null) {
            this.size -= this.safeSizeOf(key, remove);
        }
        return remove;
    }
    
    protected void entryEvicted(final K k, final V v) {
    }
    
    protected V create(final K k) {
        return null;
    }
    
    private int safeSizeOf(final K obj, final V obj2) {
        final int size = this.sizeOf(obj, obj2);
        if (size < 0) {
            throw new IllegalStateException(": " + obj + "=" + obj2);
        }
        return size;
    }
    
    protected int sizeOf(final K k, final V v) {
        return 1;
    }
    
    public final synchronized void evictAll() {
        this.trimToSize(-1);
    }
    
    public final synchronized int size() {
        return this.size;
    }
    
    public final synchronized int maxSize() {
        return this.maxSize;
    }
    
    public final synchronized int hitCount() {
        return this.hitCount;
    }
    
    public final synchronized int missCount() {
        return this.missCount;
    }
    
    public final synchronized int createCount() {
        return this.createCount;
    }
    
    public final synchronized int putCount() {
        return this.putCount;
    }
    
    public final synchronized int evictionCount() {
        return this.evictionCount;
    }
    
    public final synchronized Map<K, V> snapshot() {
        return new LinkedHashMap<K, V>((Map<? extends K, ? extends V>)this.map);
    }
}
