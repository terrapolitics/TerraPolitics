

@NetworkEventMeta(direction = EventDirection.C2S)
public class ElevatorActionEvent extends BlockEventPacket {

	public ElevatorActionEvent() {}

	public ElevatorActionEvent(int dimension, int xCoord, int yCoord, int zCoord, PlayerMovementEvent.Type type) {
		super(dimension, xCoord, yCoord, zCoord);
		this.type = type;
	}

	public PlayerMovementEvent.Type type;

	@Override
	protected void readFromStream(DataInput input) throws IOException {
		super.readFromStream(input);
		int typeId = ByteUtils.readVLI(input);
		type = PlayerMovementEvent.Type.VALUES[typeId];
	}

	@Override
	protected void writeToStream(DataOutput output) throws IOException {
		super.writeToStream(output);
		ByteUtils.writeVLI(output, type.ordinal());
	}
}
