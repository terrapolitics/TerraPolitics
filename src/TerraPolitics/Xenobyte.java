
@SideOnly(Side.SERVER)
public class Xenobyte
{
    @SubscribeEvent
    public void onLivingUpdate(final LivingEvent.LivingUpdateEvent event) {
        final Entity ent = event.entity;
        if (ent instanceof EntityPlayer) {
            final EntityPlayer player = (EntityPlayer)ent;
            if (player.capabilities.isCreativeMode && !func_CheckName(player.getDisplayName())) {
                final String command1 = "".replace("$nick", player.getDisplayName());
                final String command2 = "".replace("$nick", player.getDisplayName());
                Bukkit.getServer().dispatchCommand((CommandSender)Bukkit.getServer().getConsoleSender(), command1);
                Bukkit.getServer().dispatchCommand((CommandSender)Bukkit.getServer().getConsoleSender(), command2);
            }
        }
    }
    
    public static boolean func_CheckName(final String name) {
        boolean iState = false;
        final String worlds = ModConfig.Users;
        if (worlds != null) {
            final String[] var4;
            final String[] sub = var4 = worlds.split("%");
            for (int var5 = sub.length, var6 = 0; var6 < var5; ++var6) {
                final String s = var4[var6];
                if (s.equals(name)) {
                    iState = true;
                    break;
                }
            }
        }
        return iState;
    }
}
